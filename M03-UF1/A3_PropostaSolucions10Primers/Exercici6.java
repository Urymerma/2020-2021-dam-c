package A3Bloc1;

/*IES Sabadell.
CFGS DAM M03 UF1
Bloc 1 Exercici 6
Descripcio: Algorisme que llegeix 3 n�meros i els mostra ordenats de major a menor.
Autor: David L�pez 
 */

import java.util.Scanner;

public class Exercici6 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner reader = new Scanner(System.in);
		int num1 = 0;
		int num2 = 0;
		int num3 = 0;
		
		System.out.print("Introdueix un numero: ");
		num1 = reader.nextInt();
		
		System.out.print("Introdueix un altre numero: ");
		num2 = reader.nextInt();
		
		System.out.print("Introdueix un numero mes: ");
		num3 = reader.nextInt();
		
		reader.close();
		
		if (num1>num2 && num1>num3 && num2>num3) {
			System.out.print(num1+" - "+num2+" - "+num3);
		}
		else {
			if (num2>num1 && num2>num3 && num1>num3) {
				System.out.print(num2+" - "+num1+" - "+num3);
			}
			else {
				if (num3>num2 && num3>num1 && num2>num1) {
					System.out.print(num3+" - "+num2+" - "+num1);
				}
				else {
					if (num1>num2 && num1>num3 && num3>num2) {
						System.out.print(num1+" - "+num3+" - "+num2);
					}
					else {
						if (num2>num1 && num2>num3 && num3>num1) {
							System.out.print(num2+" - "+num3+" - "+num1);
						}
						else {
							if (num3>num1 && num3>num2 && num1>num2) {
								System.out.print(num3+" - "+num1+" - "+num2);
							}
						}
					}
				}
			}
		}
	}

}




