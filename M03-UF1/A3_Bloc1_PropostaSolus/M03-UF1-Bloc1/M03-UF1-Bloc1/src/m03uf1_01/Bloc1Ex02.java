package m03uf1_01;

//Algorisme que llegeix un número i comparar si és major que 10. 

/*Institut Sabadell. 
  CFGS DAM  M03 UF1
  Bloc 1 Exercici 1 
  Descripci�: Algorisme que llegeix un n�mero per teclat, i ens diu si és major que 10 o no*/

import java.util.Scanner;  //Classe de Java que ens permet llegir el contingut d'entrada de dades

public class Bloc1Ex02 {
	
	public static void main (String [] args) {
		Scanner reader = new Scanner(System.in);  //Creem un objecte que treballa sobre la consola (System.in)
		int num;	//Variable on posar� el n�mero llegit per teclat
		
		System.out.print("Introdueix un número enter: ");
		num = reader.nextInt();  //Llegeix un n�mero de la consola i el guarda a la variable num

		if (num > 10) 
			System.out.println("El n�mero " + num + " �s major que 10");
		else
			System.out.println("El n�mero " + num + " NO �s major que 10");
		reader.close();
	}
}
