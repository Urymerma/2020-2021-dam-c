package m03uf1_01;

import java.util.Scanner;

/*
 * Autor: Jaicle Pando
 */
public class Bloc1Ex10V3 {

	public static void main(String[] args) {
		
        Scanner lector = new Scanner(System.in);
       
        System.out.println("Algorisme que llegeix dos n�meros enters"
                + " positius i diferents i ens diu si el major �s m�ltiple del menor");
       
        System.out.println("\tPrimer numero\n");
        
        int primer = lector.nextInt();
        System.out.println("\tSegon numero\n");
        
        int segon = lector.nextInt();
       
        if (primer != segon) {
            
            if (primer > segon) {
                    
                if (primer % segon == 0) {
                    System.out.println("Es multiple " + primer + " del" + segon + " .");
                } else {
                    System.out.println("El primer no es m�ltiple del segon");
                }
            } else {
                
                if (segon % primer == 0) {
                    System.out.println("Es multiple " + segon + " del" + primer + " .");
                } else {
                    System.out.println("El segon no es m�ltiple");
                }
            }
        } else {
            System.out.println("S�n iguals");
        }
    }
	
}
