package m03uf1_01;

import java.util.Scanner;

/* 1.10.- Algorisme que llegeix dos n�meros enters positius i diferents
 *  i ens diu si el major �s m�ltiple del menor, o el que �s al mateix, 
 *  que el menor �s divisor del major. 
 */

public class Bloc1Ex10 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner reader = new Scanner(System.in);

		int num1 = 0;
		int num2 = 0;


		System.out.print("Afegeix un n�mero ");
		num1 = reader.nextInt();

		System.out.print("Afegeix un n�mero ");
		num2 = reader.nextInt();
        
        if (num1>0 && num2 > 0){
                
            if(num1>num2) {
                
                if (num1%num2 == 0) {
                    System.out.println("El n�mero "+num1+" �s multiple de "+num2);
                }
                else  System.out.println("El n�mero "+num1+" no �s multiple de "+num2);
            }
            else{
                if(num2>num1){
                    
                  if(num2%num1 == 0) {
                    System.out.println("El n�mero "+num2+" �s multiple de "+num1);
                  }
                  else
                        System.out.println("El n�mero "+num2+" no �s multiple de "+num1);
                }
                else  
                    System.out.println("Els dos n�meros s�n iguals ");
                
                }
            }
            else
                System.out.println("Error, els dos n�meros han de swer positius");
        reader.close();
	}

}	


