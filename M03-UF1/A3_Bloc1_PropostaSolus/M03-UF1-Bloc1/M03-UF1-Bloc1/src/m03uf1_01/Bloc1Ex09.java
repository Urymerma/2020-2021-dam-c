package m03uf1_01;

import java.util.Scanner;

/* Institut Sabadell
 * CFGS DAM M03 UF1
 * Bloc 1 Exercici 9
 * Descripcio: Algorisme que llegeix un n�mero i ens diu si �s m�ltiple de dos o de tres o de cap d�ells.
 * Autor: Jonathan Raya Rios
 */

public class Bloc1Ex09 {
	
	public static void main(String[] args)
	{
		int a;
		Scanner reader = new Scanner(System.in);

		System.out.println("Introdueix un numero\n");
		a = reader.nextInt();

		if (a % 2 == 0)
		{
			System.out.println("\n" + a + " �s multiple de 2.");
		}
		else
		{
			System.out.println("\n" + a + " no �s multiple de 2.");
		}

		if (a % 3 == 0)
		{
			System.out.println("\n" + a + " �s multiple de 3.");
		}
		else
		{
			System.out.println("\n" + a + " no �s multiple de 3.");
		}
		reader.close();
	}
}
