package m03uf1_01;

/*Institut Sabadell. 
CFGS DAM  M03 UF1
Bloc 1 Exercici 1 
Descripci�: Algorisme que llegeix un n�mero per teclat, el multiplica per 3 i mostra el resultat  
*/
import java.util.Scanner;  //Classe de Java que ens permet llegir el contingut d'entrada de dades

public class Bloc1Ex01 {
	
	public static void main (String [] args) {
		Scanner reader = new Scanner(System.in);  //Creem un objecte que treballa sobre la consola (System.in)
		int num;	//Variable on guargar� el n�mero llegit per teclat
		
		System.out.print("Introdueix un n�mero enter: ");
		num = reader.nextInt();  //Llegeix un n�mero de la consola i el guarda a la variable num
		
		//res = num * 3;           // num *= 3;
		System.out.println("El resultat de multiplicar el n�mero per 3 �s:  " + num * 3);
		reader.close();
	}
}