package m03uf1_01;

/* Algorisme que llegeix una qualificaci� alfanum�rica amb una lletra
 *  (�M� / �I� / �S� / �B� / �N� / �E�) i el resultat mostra la qualificaci� num�rica
 *   ponderada segons la seg�ent taula.....
 */

import java.util.Scanner;  //Classe de Java que ens permet llegir el contingut d'entrada de dades

public class Bloc1Ex18 {

	public static void main (String [] args) {
		Scanner reader = new Scanner(System.in);  //Creem un objecte que treballa sobre la consola (System.in)
		
		char nota;
	
		System.out.println("Introdueix la lletra equivalent a una nota: ");
		nota = reader.next().charAt(0);		//Llegeix un caracter de teclat
		reader.nextLine();
		
		switch (nota) {
				case 'm':
				case 'M': System.out.println("Nota equivalent: 1.5");
						  break;
				case 'i':
				case 'I': System.out.println("Nota equivalent: 4");
						  break;
				case 's':
				case 'S': System.out.println("Nota equivalent: 5.5");
						  break;
				case 'b':
				case 'B': System.out.println("Nota equivalent: 6.5");
						  break;	 
				case 'n':
				case 'N': System.out.println("Nota equivalent: 8");
						  break;	 
				case 'e':
				case 'E': System.out.println("Nota equivalent: 9.5");
						  break;	  
				default: System.out.println("Nota incorrecta");
			}		
		 reader.close();
	}  //Fi del mai
}
