package m03uf1_01;
/*Institut Sabadell. 
CFGS DAM  M03 UF1
Bloc 1 Exercici 5
Descripci�: Algorisme que llegeix 3 n�meros per teclat i els escriu el m�s gran
*/
import java.util.Scanner;  //Classe de Java que ens permet llegir el contingut d'entrada de dades

public class Bloc1Ex05 {
	
	public static void main (String [] args) {
		Scanner reader = new Scanner(System.in);  //Creem un objecte que treballa sobre la consola (System.in)
		int num1 = 0;	//Variable on guardar� el n�mero llegit per teclat
		int num2 = 0;
		int num3 = 0;
		int aux;
		
		//Obtenir dades ...
		System.out.print("Introdueix un n�mero enter: ");
		num1 = reader.nextInt();  //Llegeix un n�mero de la consola i el guarda a la variable num
		num2 = reader.nextInt();  //Llegeix un n�mero de la consola i el guarda a la variable num
		num3 = reader.nextInt();  //Llegeix un n�mero de la consola i el guarda a la variable num
		
		//Tractament de les dades i resultats
			
		aux = num1;
		if (num2 > aux) {
				  aux = num2;
		 }
		 if (num3 > aux) {
				  aux = num3;
		 }	
			
       System.out.println("El n�mero " + aux + " �s el m�s gran"); 
       reader.close();
	}
}

