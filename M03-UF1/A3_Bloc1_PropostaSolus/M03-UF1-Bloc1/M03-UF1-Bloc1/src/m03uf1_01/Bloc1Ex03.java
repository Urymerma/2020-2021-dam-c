package m03uf1_01;

/*Institut Sabadell. 
CFGS DAM  M03 UF1
Bloc 1 Exercici 3 
Descripci�: Algorisme que llegeix dos n�meros per teclat, i respon quin �s el m�s gran  
*/
import java.util.Scanner;  //Classe de Java que ens permet llegir el contingut d'entrada de dades

public class Bloc1Ex03 {
	
	public static void main (String [] args) {
		Scanner reader = new Scanner(System.in);  //Creem un objecte que treballa sobre la consola (System.in)
		int num1 = 0;	//Variable on posar� el n�mero llegit per teclat
		int num2 = 0;
		
		//Obtenir dades ...
		System.out.print("Introdueix un n�mero enter: ");
		num1 = reader.nextInt();  //Llegeix un n�mero de la consola i el guarda a la variable num

		System.out.print("Introdueix un altre n�mero enter: ");
		num2 = reader.nextInt();  //Llegeix un número de la consola i el guarda a la variable num
		
		//Tractament de les dades i resultats
		if (num1 > num2) {
			System.out.println("El n�mero " + num1 + " �s major que " + num2);
		}
		else {
			if (num2 > num1) {
					System.out.println("El n�mero " + num2 + " �s major que " + num1);
			}
			else {
				System.out.println("Els dos n�meros s�n iguals");
			}
		}	
		
		reader.close();
	}
}