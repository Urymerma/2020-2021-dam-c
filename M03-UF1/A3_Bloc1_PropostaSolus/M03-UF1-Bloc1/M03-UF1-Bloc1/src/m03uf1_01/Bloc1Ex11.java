package m03uf1_01;

import java.util.Scanner;

/* Institut Sabadell
 * CFGS DAM M03 UF1
 * Bloc 1 Exercici 11
 * Descripcio: Algorisme que llegeix un n�mero i ens diu si �s parell o senar. 
 * Autor: Jonathan Raya Rios
 */

public class Bloc1Ex11 {

	public static void main(String[] args) 
	{
		int a = 0;
		Scanner reader = new Scanner(System.in);
		
		System.out.println("Introdueix  un n�mero:\n");
		a = reader.nextInt();
		
		if (a % 2 == 0) 
		{
			System.out.println("\n" + a + " �s parell.");
		}
		else
		{
			System.out.println("\n" + a + " �s senar.");
		}
		reader.close();
	}
}
