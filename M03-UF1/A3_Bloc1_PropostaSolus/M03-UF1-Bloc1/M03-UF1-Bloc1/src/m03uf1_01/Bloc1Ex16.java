package m03uf1_01;

import java.util.Scanner;

/* Ex 1.16.- Llegeix una qualificaci� num�rica i escriu com a resultat la mateixa 
 * qualificaci� per� alfab�ticament.
 */

public class Bloc1Ex16 {
	
	public static void main (String [] args) {
		Scanner reader = new Scanner(System.in);  //Creem un objecte que treballa sobre la consola (System.in)

		double nota = 0;
		//Obtenir dades ...
		System.out.print("Introdueix la nota: ");
		nota = reader.nextDouble();
		if (nota >= 0 && nota < 5)
			System.out.println("Estas Suspes");
		else {
			if (nota < 6)
				System.out.println("Tens un Suficient");
			else {
				if (nota < 7)
					System.out.println("Tens un Be");
				else {
					if (nota < 9)
						System.out.println("Tens un Notable");
					else {
						if (nota < 10)
							System.out.println("Tens un Excelent");
						else {
							if (nota == 10) 
								System.out.println("Tens Matricula d'Honor");
							else
								System.out.println("Error, nota no v�lida");
						}
					}
				}
			}
		}
		reader.close();
	}  //Fi del main
}
