package m03uf1_01;

import java.util.Scanner;

/* Ex 1.16.- Llegeix una qualificaci� num�rica i escriu com a resultat la mateixa 
 * qualificaci� per� alfab�ticament.
 */

public class Bloc1Ex16V2 {

	public static void main (String [] args) {
		Scanner reader = new Scanner(System.in);  //Creem un objecte que treballa sobre la consola (System.in)

		int nota = 0;
		//Obtenir dades ...
		System.out.print("Introdueix la nota: ");
		nota = reader.nextInt();

		switch (nota) {
            case 0:
            case 1:
            case 2:
            case 3: System.out.println ("Molt Deficient");
                    break;
            case 4: System.out.println("Insuficient");
                    break;
            case 5: System.out.println("Suficient");
                    break;
            case 6: System.out.println("B�");
                    break;
            case 7:
            case 8: System.out.println("Notable");
                    break;
            case 9:
            case 10: System.out.println("Excel.lent");
                     break;
            default: System.out.println("nota no correcta");

		}

/*		if (nota < 5)
			System.out.println("Estas Suspes");
		else {
			if (nota < 6)
				System.out.println("Tens un Suficient");
			else {
				if (nota < 7)
					System.out.println("Tens un Be");
				else {
					if (nota < 9)
						System.out.println("Tens un Notable");
					else {
						if (nota < 10)
							System.out.println("Tens un Excelent");
						else
							System.out.println("Tens Matricula d'Honor");
					}
				}
			}
		}
*/
		reader.close();
	}  //Fi del main
}
