package m03uf1_01;

import java.util.Scanner;

/* 1.14.- Algorisme que llegeix una quantitat per teclat i
 *  si �s superior a 500 li fa el 10% de descompte,
 *  en cas contrari �nicament li fa el 5%. 
 *  El resultat cal que ens digui la quantitat descomptada i 
 *  la quantitat a pagar.
 */


public class Bloc1Ex14 {
	
	public static void main (String [] args) {
		Scanner reader = new Scanner(System.in);  //Creem un objecte que treballa sobre la consola (System.in)
		
		final float MAX = 500;   //MAX �s una constant
		float quantitat, descompte;
		
		//Obtenir dades ...
		System.out.print("Introdueix una quantitat: ");
		quantitat = reader.nextFloat();  	
			 
		if (quantitat > MAX) 
			descompte = quantitat * 0.10f;
		else 
			descompte = quantitat * 0.05f;
			 
		System.out.println("A aquesta quantitat li correspon un descompte de " + descompte);
		System.out.println("El preu final es de " + (quantitat - descompte));

		System.out.printf("A aquesta quantitat li correspon un descompte de %.2f. El preu final es de %.2f\n", descompte, (quantitat - descompte) );
		
		reader.close();
	
	}  //Fi del main
}
