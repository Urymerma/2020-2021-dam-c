package m03uf1_01;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Scanner;

/* Institut Sabadell
 * CFGS DAM M03 UF1
 * Bloc 1 Exercici 12
 * Descripcio: Algorisme que llegint una data de naixement i ens digui si pot votar (t� 18 anys o m�s).
 * Autor: Jonathan Raya Rios
 */

public class Bloc1Ex12 {

	public static void main(String[] args)
	{
		Scanner reader = new Scanner(System.in);
		Calendar data = new GregorianCalendar();
		int anyUsuari = 0;
		int anyActual = data.get(Calendar.YEAR);
		int mesUsuari = 0;
		int mesActual = data.get(Calendar.MONTH) + 1;
		int diaUsuari = 0;
		int diaActual = data.get(Calendar.DAY_OF_MONTH);
		boolean correcte = true;

		System.out.println("Introdueix el teu dia de naixement: \n");
		diaUsuari = reader.nextInt();
		System.out.println("\nIntrodueix el teu mes de naixement: \n");
		mesUsuari = reader.nextInt();
		System.out.println("\nIntrodueix el teu any de naixement: \n");
		anyUsuari = reader.nextInt();


        //Validaci� de la data de naixament de l'usuari
		if (diaUsuari < 1 || diaUsuari > 31) {
            correcte = false;
            System.out.println("Dia no correcte");
		}

/// if (mesUsuari >= 1 && mesUsuari <= 12) {
///     if (mesUsuari ==  2 && diaUsuari > 28) {
///             correcte = false;
///             System.out.println("Aquest mes no pot tenir m�s de 28 dies");
///     }
///     else if ((mesUsuari ==  4 || mesUsuari ==  6 || mesUsuari ==  9 || mesUsuari ==  11) && diaUsuari > 30) {
///                  correcte = false;
///                  System.out.println("Aquest mes no pot tenir m�s de 30 dies");
///          }
/// }
/// else {
///     correcte = false;
///     System.out.println("No has posat un n�mero de mes v�lid");
/// }
		

        if (correcte == true){
            switch(mesUsuari){
                    case 1:
                    case 3:
                    case 5:
                    case 7:
                    case 8:
                    case 10:
                    case 12: break;
                    case 2:  if (diaUsuari > 28) {
                                correcte = false;
                                System.out.println("Aquest mes no pot tenir m�s de 28 dies");
                             }
                             break;
                    case 4:
                    case 6:
                    case 9:
                    case 11: if (diaUsuari > 30) {
                                correcte = false;
                                System.out.println("Aquest mes no pot tenir m�s de 30 dies");
                             }
                             break;
                    default: correcte = false;
                             System.out.println("No has posat un n�mero de mes v�lid");
                }

        }
		if (correcte == true && (anyUsuari < 1900 || anyUsuari > anyActual)) {
            correcte = false;
            System.out.println("Any incorrecte");
		}
        //Fi de la validaci� de la data


		if (correcte == true) {
			System.out.println("\n" + "Avui �s " + diaActual + "/" + mesActual + "/" + anyActual + "\n");
			if (anyActual - anyUsuari > 18)
			{
				System.out.println("\n" + "Vas neixer el " + diaUsuari + "/" + mesUsuari + "/" + anyUsuari + " aix� que ets major d'edat.");
			}
			else if (anyActual - anyUsuari == 18)
			{
				if (mesUsuari < mesActual)
				{
					System.out.println("\n" + "Vas neixer el " + diaUsuari + "/" + mesUsuari + "/" + anyUsuari + " aix� que ets major d'edat.");
				}
				else if (mesUsuari == mesActual)
				{
					if (diaUsuari <= diaActual)
					{
						System.out.println("\n" + "Vas neixer el " + diaUsuari + "/" + mesUsuari + "/" + anyUsuari + " aix� que ets major d'edat.");
					}
					else
					{
						System.out.println("\n" + "Vas neixer el " + diaUsuari + "/" + mesUsuari + "/" + anyUsuari + " aix� que no ets major d'edat.");
					}
				}
				else
				{
					System.out.println("\n" + "Vas neixer el " + diaUsuari + "/" + mesUsuari + "/" + anyUsuari + " aix� que no ets major d'edat.");
				}
			}
			else
			{
				System.out.println("\n" + "Vas neixer el " + diaUsuari + "/" + mesUsuari + "/" + anyUsuari + " aix� que no ets major d'edat.");
			}
		}
		else
		{
			System.out.println("\nIntrodueix una data valida.");
		}
		reader.close();
	}
}
